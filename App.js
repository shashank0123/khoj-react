import React from 'react';
import { StyleSheet,AsyncStorage,Image, Text, View } from 'react-native';
import { createStackNavigator,createSwitchNavigator,createBottomTabNavigator,createDrawerNavigator, createAppContainer, DrawerItems } from "react-navigation";
import { Ionicons } from '@expo/vector-icons';
import { Container,Content, Header } from 'native-base';

import Login from './screens/Login';
import Register from './screens/Register';
import Account from './screens/Account';
import Dashboard from './screens/Dashboard';
import Series from './screens/Series';
import Library from './screens/Library';
import Search from './screens/Search';
import Filter from './screens/Filter';
import ForgotPassword from './screens/ForgotPassword';
import Wishlist from './screens/Wishlist';
import Profile from './screens/Profile';
import EditProfile from './screens/EditProfile';
import AuthLoadingScreen from './screens/AuthLoadingScreen';
import SeriesDetails from './screens/SeriesDetails';
import ChangePassword from './screens/ChangePassword';
import Privacy from './screens/Privacy';
import FAQs from './screens/FAQs';
import Feedback from './screens/Feedback';
import FullSearch from './screens/FullSearch';

import { TabBar } from "react-native-animated-nav-tab-bar";
import Icon from 'react-native-vector-icons/Feather';

const BAppNav=createBottomTabNavigator({
  Movies: {screen: Dashboard,navigationOptions:{  
    tabBarLabel:'Movies',  
    tabBarIcon: ({focused}) =>(
      focused
            ? <Image source={require('./assets/home.png')}  style={{width:35, height:35}}  />
            : <Image source={require('./assets/home.png')}  style={{width:30, height:30}}  /> 
    ) 
  }},
  Search: {screen: Search,navigationOptions:{  
    tabBarLabel:'Search',  
    tabBarIcon: ({focused}) =>(
      focused
            ? <Image source={require('./assets/search.png')}  style={{width:35, height:35}}  />
            : <Image source={require('./assets/search.png')}  style={{width:30, height:30}}  /> 
    ) 
  }},
  Library: {screen: Library,navigationOptions:{  
    tabBarLabel:'Favorites',  
    tabBarIcon: ({focused}) =>(
      focused
            ? <Image source={require('./assets/favs.png')}  style={{width:35, height:35}}  />
            : <Image source={require('./assets/favs.png')}  style={{width:30, height:30}}  /> 
    ) 
  }},
  Wishlist: {screen: Wishlist,navigationOptions:{  
    tabBarLabel:'Wishlist',  
    tabBarIcon: ({focused}) =>(
      focused
            ? <Image source={require('./assets/wish.png')}  style={{width:35, height:35}}  />
            : <Image source={require('./assets/wish.png')}  style={{width:30, height:30}}  /> 
    ) 
  }},
  Filter: {screen: Filter,navigationOptions:{  
    tabBarLabel:'Filter',  
    tabBarIcon: ({focused}) =>(
      focused
            ? <Image source={require('./assets/filter.png')}  style={{width:35, height:35}}  />
            : <Image source={require('./assets/filter.png')}  style={{width:30, height:30}}  /> 
    ) 
  }},
},{
tabBarOptions: {
  activeTintColor: "#fff",      
  inactiveTintColor: "grey",  
  style: {backgroundColor:'#000',height:60},
  activeTabStyle: {
    borderTopColor:'#01c72c',borderTopWidth:2
  }
}
});

async function getName(){
  const uname =  await AsyncStorage.getItem('name');
  return uname;
}

const MainNavigator = createStackNavigator({
  //AuthLoadingScreen:AuthLoadingScreen,
  Dashboard:{screen:BAppNav,navigationOptions: {
    header: null,
  }},
  Login:{screen:Login,navigationOptions: {header: null}},
  Feedback:{screen:Feedback,navigationOptions: {header: null}},
  FAQs:{screen:FAQs,navigationOptions: {header: null}},
  FullSearch:{screen:FullSearch,navigationOptions: {header: null}},
  Privacy:{screen:Privacy,navigationOptions: {header: null}},
  Account:{screen:Account,navigationOptions: {header: null}},
  ChangePassword:{screen:ChangePassword,navigationOptions: {header: null}},
  EditProfile:{screen:EditProfile,navigationOptions: {header: null}},
  Profile:{screen:Profile,navigationOptions: {header: null}},
  SeriesDetails:{screen:SeriesDetails,navigationOptions: {header: null}},
  Register:{screen:Register,navigationOptions: {header: null}},
  ForgotPassword:{screen:ForgotPassword,navigationOptions: {header: null}},
},{
  defaultNavigationOptions:({navigation})=>{
    return{ 
    headerLeft:<Ionicons style={{marginLeft:15,color:'#fff'}} onPress={()=>navigation.toggleDrawer()} name="md-menu" size={30}/>,
    headerTintColor:'#fff',
    headerStyle:{backgroundColor:'#2f77e1'},
    headerTitle:'KHOJ',
    headerRight:<Ionicons  onPress={()=>logout().then(navigation.navigate("Login")) }style={{marginRight:10,color:'#fff'}}
     name="md-log-out" size={25}/>
  }}
})

async function logout(){
  await AsyncStorage.removeItem('LoggedIn');
}

const App = createAppContainer(MainNavigator);

export default App;